{
	"code": "1000",
	"data": {
		"menus": [{
			"id": 11,
			"name": "首页",
			"level": 0,
			"childNode": [{
				"id": 25,
				"name": "首页",
				"parentId": 11,
				"level": 1,
				"functions": [{
					"id": 16,
					"menuId": 25,
					"code": "16",
					"name": "查看"
				}],
				"path": "/Home/Index"
			},{
				"id": 25,
				"name": "店铺管理",
				"parentId": 11,
				"level": 1,
				"functions": [{
					"id": 16,
					"menuId": 25,
					"code": "16",
					"name": "查看"
				}],
				"path": "/Home/StoreManagement/StoreManagementList"
			},{
				"id": 25,
				"name": "数据统计",
				"parentId": 11,
				"level": 1,
				"functions": [{
					"id": 16,
					"menuId": 25,
					"code": "16",
					"name": "查看"
				}],
				"path": "/Home/DataStatistics/Index"
			},{
				"id": 25,
				"name": "用户统计",
				"parentId": 11,
				"level": 1,
				"functions": [{
					"id": 16,
					"menuId": 25,
					"code": "16",
					"name": "查看"
				}],
				"path": "/Home/UserStatistics/Index"
			},{
				"id": 25,
				"name": "意见反馈",
				"parentId": 11,
				"level": 1,
				"functions": [{
					"id": 16,
					"menuId": 25,
					"code": "16",
					"name": "查看"
				}],
				"path": "/Home/Feedback"
			}],
			"path": "",
			"icon": "icon-pingjia"
		}]
	}
}