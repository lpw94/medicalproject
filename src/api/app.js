const app = {
  globalSearch: {
    // 参数 tCode (首页热门:syss 商城热门:scss）
    findTypeByTcode: '/b/config/search/findTypeByTcode.html',
  },
  // 商品管理
  selectGoodsList: '/b/goods/selectGoodsList.html',
}

export default app

const getSign = app.globalSearch.findTypeByTcode

export { getSign }