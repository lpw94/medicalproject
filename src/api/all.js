import user from './user'

const api = {
  ...user,
}

export default (Vue) => {
  Vue.api = Vue.prototype.$api = api
}
