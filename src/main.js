import 'babel-polyfill'
import '@/styles/index.less'
import '@/utils/rem'
import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import httpCode from '@/utils/httpCode'
import api from '@/api/all'
import axios from '@/utils/axios'
import upload from '@/utils/upload'
import { imgView, htmlStrRemoveIframe } from '@/utils/utils'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Distpicker from 'vue-distpicker'
import iUpload from '@/components/iUpload'
import goodsList from '@/components/goodsList'
import selectGoods from '@/components/selectGoods'
import addressOption from '@/utils/addressOption'
import VueClipboard from 'vue-clipboard2'

Vue
  .use(ElementUI)
  .use(Distpicker)
  .use(api)
  .use(httpCode)
  .use(axios)
  .use(imgView)
  .use(upload)
  .use(addressOption)
  .use(VueClipboard)
  .use(htmlStrRemoveIframe)

Vue.component('i-upload', iUpload)
Vue.component('goods-list', goodsList)
Vue.component('select-goods', selectGoods)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<router-view/>'
})
