import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {
    path: '/',
    component: () => import('@/views/Login')
  },
  {
    path: '/Index',
    component: () => import('@/views/Layout'),
    children: [
      {
        path: '/blank',
        component: () => import('@/views/blank')
      },
      //首页
      {
        path: '/Home/Index',
        component: () => import('@/views/Home/Index'),
        meta: ['首页']
      },
      // 店铺管理
      {
        path: '/Home/StoreManagement/StoreManagementList',
        component: () => import('@/views/Home/StoreManagement/StoreManagementList'),
        meta: ['店铺管理']
      },
      // 店铺商品明细
      {
        path: '/Home/StoreManagement/StoreGoodsDetail',
        component: () => import('@/views/Home/StoreManagement/StoreGoodsDetail'),
        meta: ['店铺管理','店铺商品明细']
      },
      // 店铺销售明细
      {
        path: '/Home/StoreManagement/StoreOrderDetails',
        component: () => import('@/views/Home/StoreManagement/StoreOrderDetails'),
        meta: ['店铺管理','店铺销售明细']
      },
      // 店铺停用详情
      {
        path: '/Home/StoreManagement/storeDisableDetails',
        component: () => import('@/views/Home/StoreManagement/storeDisableDetails'),
        meta: ['店铺管理','店铺停用详情']
      },
      // 数据统计
      {
        path: '/Home/DataStatistics/Index',
        component: () => import('@/views/Home/DataStatistics/Index'),
        meta: ['数据统计']
      },
      // 用户统计
      {
        path: '/Home/UserStatistics/Index',
        component: () => import('@/views/Home/UserStatistics/Index'),
        meta: ['数据统计']
      },
      // 意见反馈
      {
        path: '/Home/Feedback',
        component: () => import('@/views/Home/Feedback'),
        meta: ['数据统计']
      },
      {
        path: '/Home/Feedback1',
        component: () => import('@/views/Home/Feedback1'),
        meta: ['数据统计']
      },
      {
        path: '/Home/Feedback2',
        component: () => import('@/views/Home/Feedback2'),
        meta: ['数据统计']
      },
      {
        path: '/Home/Feedback3',
        component: () => import('@/views/Home/Feedback3'),
        meta: ['数据统计']
      },
      {
        path: '/Home/Feedback4',
        component: () => import('@/views/Home/Feedback4'),
        meta: ['症候管理']
      },

      {
        path: '/Home/Feedback5',
        component: () => import('@/views/Home/Feedback5'),
        meta: ['疾病管理']
      },
      {
        path: '/Home/Feedback6',
        component: () => import('@/views/Home/Feedback6'),
        meta: ['症候疾病关联表象']
      },
      {
        path: '/Home/Feedback7',
        component: () => import('@/views/Home/Feedback7'),
        meta: ['配方管理']
      },
      {
        path: '/Home/Feedback8',
        component: () => import('@/views/Home/Feedback8'),
        meta: ['方剂管理']
      },
      {
        path: '/Home/Feedback9',
        component: () => import('@/views/Home/Feedback9'),
        meta: ['医嘱管理']
      },
      {
        path: '/Home/Feedback10',
        component: () => import('@/views/Home/Feedback10'),
        meta: ['治疗紧急程度设置']
      },
      {
        path: '/Home/Feedback11',
        component: () => import('@/views/Home/Feedback11'),
        meta: ['规则设置']
      },
      {
        path: '/Home/Feedback12',
        component: () => import('@/views/Home/Feedback12'),
        meta: ['系统设置']
      },
      // 用户管理
      {
        path: '/UserManage/UserList',
        component: () => import('@/views/UserManage/UserList'),
        meta: ['用户管理', '用户列表']
      },
      {
        path: '/UserManage/UserGroup',
        component: () => import('@/views/UserManage/UserGroup'),
        meta: ['用户管理', '用户组']
      },
      {
        path: '/UserManage/UserWithdrawCash',
        component: () => import('@/views/UserManage/UserWithdrawCash/Index'),
        meta: ['用户管理', '用户提现记录']
      },
      {
        path: '/UserManage/UserGrouping',
        component: () => import('@/views/UserManage/UserGrouping/Index'),
        meta: ['用户管理', '用户分组']
      },
      // 评价管理
      {
        path: '/EvaluateManage/EvaluateList',
        component: () => import('@/views/EvaluateManage/EvaluateList'),
        meta: ['评价管理']
      },
      //财务管理
      {
        path: '/FinancialManage/Index',
        component: () => import('@/views/FinancialManage/Index'),
        meta: ['财务管理']
      },
      {
        path: '/FinancialManage/RefundList',
        component: () => import('@/views/FinancialManage/RefundList'),
        meta: ['财务管理']
      },
      {
        path: '/FinancialManage/RefundManage',
        component: () => import('@/views/FinancialManage/RefundManage'),
        meta: ['财务管理']
      },
      {
        path: '/FinancialManage/TxList',
        component: () => import('@/views/FinancialManage/TxList'),
        meta: ['财务管理']
      },
      //权限管理
      {
        path: '/RoleManage/Log',
        component: () => import('@/views/RoleManage/Log'),
        meta: ['权限管理']
      },
      {
        path: '/RoleManage/Manage/Index',
        component: () => import('@/views/RoleManage/Manage/Index'),
        meta: ['权限管理']
      },
      {
        path: '/RoleManage/memberManage/Index',
        component: () => import('@/views/RoleManage/memberManage/Index'),
        meta: ['权限管理']
      },
       //发布
      {
        path: '/Issue/Index',
        component: () => import('@/views/Issue/Index'),
        meta: ['发布']
      },
      //供应商
      {
        path: '/Supply/SupplyManage/Index',
        component: () => import('@/views/Supply/SupplyManage/Index'),
        meta: ['供应商']
      },
      {
        path: '/Supply/SupplyCheck/Index',
        component: () => import('@/views/Supply/SupplyCheck/Index'),
        meta: ['供应商']
      },
      {
        path: '/Supply/StoreManage/Index',
        component: () => import('@/views/Supply/StoreManage/Index'),
        meta: ['供应商']
      },
      {
        path: '/Supply/StoreManage/OfferList',
        component: () => import('@/views/Supply/StoreManage/OfferList'),
        meta: ['供应商']
      },
      {
        path: '/Supply/StoreManage/OrderList',
        component: () => import('@/views/Supply/StoreManage/OrderList'),
        meta: ['供应商']
      },
      // 商品
      // 报价单管理
      {
        path: '/Goods/Quotes/QuotesManagement',
        component: () => import('@/views/Goods/Quotes/QuotesManagement'),
        meta: ['报价单管理']
      },
      // 报价单详情
      {
        path: '/Goods/Quotes/QuotesDetail',
        component: () => import('@/views/Goods/Quotes/QuotesDetail'),
        meta: ['报价单详情']
      },
      // 商品管理
      {
        path: '/Goods/GoodsManagement/GoodsManagement',
        component: () => import('@/views/Goods/GoodsManagement/GoodsManagement'),
        meta: ['商品管理']
      },
      // 未通过审核的商品
      {
        path: '/Goods/GoodsManagement/UnreviewedDetail',
        component: () => import('@/views/Goods/GoodsManagement/UnreviewedDetail'),
        meta: ['未通过审核的商品']
      },
      // 停用的商品
      {
        path: '/Goods/GoodsManagement/Disabledetail',
        component: () => import('@/views/Goods/GoodsManagement/Disabledetail'),
        meta: ['停用的商品']
      },
      // 订单
      // 订单管理
      {
        path: '/Order/OrderManagement',
        component: () => import('@/views/Order/OrderManagement'),
        meta: ['订单管理']
      },
      // 售后管理
      {
        path: '/Order/AfterSaleManagement',
        component: () => import('@/views/Order/AfterSaleManagement'),
        meta: ['订单管理']
      },
      // 评价
      // PC端评价管理
      {
        path: '/Appraise/PC/AppraiseManagement',
        component: () => import('@/views/Appraise/PC/AppraiseManagement'),
        meta: ['PC端评价管理']
      },
      // APP端评价管理
      {
        path: '/Appraise/APP/AppraiseManagement',
        component: () => import('@/views/Appraise/APP/AppraiseManagement'),
        meta: ['APP端评价管理']
      },
      // APP端评价审核
      {
        path: '/Appraise/APP/AppraiseAudit',
        component: () => import('@/views/Appraise/APP/AppraiseAudit'),
        meta: ['APP端评价审核']
      },
      // 会员
      // 会员管理
      {
        path: '/Member/MemberManagement',
        component: () => import('@/views/Member/MemberManagement'),
        meta: ['会员管理']
      },
      // 积分设置
      {
        path: '/Member/IntegralSet',
        component: () => import('@/views/Member/IntegralSet'),
        meta: ['积分设置']
      },
      // 会员等级设置
      {
        path: '/Member/LevelSet',
        component: () => import('@/views/Member/LevelSet'),
        meta: ['会员等级设置']
      },
      // 会员详情
      {
        path: '/Member/PersonalDetail',
        component: () => import('@/views/Member/PersonalDetail'),
        meta: ['会员详情']
      },
      // 采购
      // 采购管理
      {
        path: '/Purchase/PurchaseManagement',
        component: () => import('@/views/Purchase/PurchaseManagement'),
        meta: ['采购管理']
      },
      // 购物详情
      {
        path: '/Purchase/UserGuide',
        component: () => import('@/views/Purchase/UserGuide'),
        meta: ['购物详情']
      },
      // 消费明细
      {
        path: '/Purchase/PurchaseDetails',
        component: () => import('@/views/Purchase/PurchaseDetails'),
        meta: ['消费明细']
      },
      // 采购审核
      {
        path: '/Purchase/APAuditor/Index',
        component: () => import('@/views/Purchase/APAuditor/Index'),
        meta: ['采购审核']
      },
      // 采购单审核
      {
        path: '/Purchase/PurchaseOrderAudit',
        component: () => import('@/views/Purchase/PurchaseOrderAudit'),
        meta: ['采购商审核']
      },
      // 推广
      // App设置
      {
        path: '/Generalize/AppSet/AppSet',
        component: () => import('@/views/Generalize/AppSet/AppSet'),
        meta: ['App设置']
      },
      // 专题推荐设置
      {
        path: '/Generalize/AppSet/LivelySet',
        component: () => import('@/views/Generalize/AppSet/LivelySet'),
        meta: ['专题推荐设置']
      },
      // 专题分类
      {
        path: '/Generalize/AppSet/CategorySet',
        component: () => import('@/views/Generalize/AppSet/CategorySet'),
        meta: ['专题分类']
      },
      // bannat设置
      {
        path: '/Generalize/AppSet/BannarSet',
        component: () => import('@/views/Generalize/AppSet/BannarSet'),
        meta: ['bannat设置']
      },
      // 首页弹窗设置
      {
        path: '/Generalize/AppSet/PopupSet',
        component: () => import('@/views/Generalize/AppSet/PopupSet'),
        meta: ['首页弹窗设置']
      },
      // 推广设置
      // 展位设置
      {
        path: '/Generalize/GeneralizeSet/BoothSet',
        component: () => import('@/views/Generalize/GeneralizeSet/BoothSet'),
        meta: ['首页弹窗设置']
      },
    ]
  }
]

const router = new Router({
  mode: 'hash',
  routes: routes
})

router.beforeEach((to, from, next) => {
  if (!window.sessionStorage.token && to.path !== '/') {
    // window.location.replace(window.location.origin)
    next('/')
  } else {
    next()
  }
})

export default router
