/**
 * 图片预览方法
 * 已注入所有Vue实例，
 * template模板里调用 $imgPreview(src)
 * 组件方法里调用 this.$imgPreview(src)
 */

const imgView = Vue => {
  const scaleImg = src => {
    let div = document.createElement('div')
    let img = document.createElement('img')
    let close = document.createElement('i')
    div.className = 'body__img__preview imgView-ani'
    img.src = src
    close.className = 'body__img__close'
    close.onclick = () => {
      document.body.removeChild(div)
    }
    img.onclick = () => {
      document.body.removeChild(div)
    }
    div.appendChild(img)
    div.appendChild(close)
    document.body.appendChild(div)
  }
  Vue.prototype.$imgView = scaleImg
  Vue.imgView = scaleImg
}

const htmlStrRemoveIframe = Vue => {
  const fun = (htmlStr = '') => {
    return htmlStr.replace(/<iframe.{1,}<\/iframe>/, '')
  }
  Vue.prototype.$htmlStrRemoveIframe = fun
  Vue.htmlStrRemoveIframe = fun
}

export { imgView, htmlStrRemoveIframe }
