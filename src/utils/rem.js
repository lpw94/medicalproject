(function () {
  document.body.style.fontSize = '16px'
  const setRem = () => {
    let vw = document.documentElement.clientWidth
    vw > 1920 && (vw = 1920)
    document.documentElement.style.fontSize = vw / 19.2 + 'px'
  }
  setRem()
  window.onresize = setRem
}())
