import { pca, pcaa } from 'area-data'

// const pcaOption = a()

const pcaOption = (() => {
// 解析全国省市数据
  let pcaOption = []
  let data = pca[86]
  for (let key in data) {
    let children = []
    for (let i in pca[key]) {
      children.push({
        label: pca[key][i],
        value: pca[key][i]
      })
    }
    pcaOption.push({
      label: data[key],
      value: data[key],
      children: children
    })
  }
  return pcaOption
})()

// 解析地址option 省市区
// const pcaaOption = b()

const pcaaOption = (() => {
  let pcaaOption = []
  for (let key in pcaa[86]) {
    let children1 = []
    let v1 = pcaa[key]
    for (let i in v1) {
      let children2 = []
      let v2 = pcaa[i]
      for (let j in v2) {
        children2.push({
          label: v2[j],
          value: v2[j]
        })
      }
      children1.push({
        label: v1[i],
        value: v1[i],
        children: children2
      })
    }
    pcaaOption.push({
      label: pcaa[86][key],
      value: pcaa[86][key],
      children: children1
    })
  }
  return pcaaOption
})()

const addressOption = {
  pca: pcaOption,
  pcaa: pcaaOption
}

export default (Vue) => {
  Vue.addressOption = Vue.prototype.$addressOption = addressOption
}
