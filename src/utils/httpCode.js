const http = {
  OK: '1000'
}

export default (Vue) => {
  Vue.prototype.http = http

  Vue.http = http
}
