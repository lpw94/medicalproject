import Vue from 'vue'
// type为date,return yy-MM-dd
// type为dateTime, return yy-MM-dd hh:mm:ss
// type为time, return hh:mm:ss
Vue.filter('formatDate', function (type, value) {
  if (typeof (value) === 'string') value = value.replace(/-/g, '/')
  let d = new Date(value)
  let Y = d.getFullYear()
  let M = d.getMonth() < 9 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1
  let D = d.getDate() < 10 ? '0' + d.getDate() : d.getDate()
  let h = d.getHours()
  let m = d.getMinutes()
  let s = d.getSeconds()
  // let ds =  `${Y}${M}${D}${h}${m}${s}`
  if (type === 'date') {
    return Y + '-' + M + '-' + D
  } else if (type === 'dateTime') {
    return Y + '-' + M + '-' + D + ' ' + h + ':' + m + ':' + s
  } else {
    return h + ':' + m + ':' + s
  }
})
