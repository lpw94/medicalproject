import axios from 'axios'
import qs from 'qs'

let SERVICE_URL = ''
let REFUND_URL = ''
let WEBSOCKET_URL = ''

/* eslint-disable */
if (false && process.env.NODE_ENV === 'development') {
  // 开发环境
  // API服务器地址
  SERVICE_URL = '//120.78.133.107:8400/'  
  // 退款API地址
  REFUND_URL = '//testwlyxapi.xlhbank.com'
  // WEBSOCKS通信地址
  WEBSOCKET_URL = '//120.78.133.107:8600'
} else if ( true || process.env.SERVICE_ENV === 'test') {
  // 测试环境  当前开发入口
  //SERVICE_URL = '//47.106.160.2:8400/'
  // 测试环境  当前开发入口
  SERVICE_URL = '//47.106.9.169:7300/'
  
  REFUND_URL = '//testwlyxapi.xlhbank.com'
  WEBSOCKET_URL = '//47.106.160.2:8600'
} else {
  // 正式环境
  SERVICE_URL = '//wlyxmanageapi.xlhbank.com'
  REFUND_URL = '//wlyxapi.xlhbank.com'
  WEBSOCKET_URL = 'wlyxsocket.xlhbank.com'
}

const axiosX = axios.create({
  baseURL: SERVICE_URL
})
const reFundPost = axios.create({
  baseURL: REFUND_URL
})
const XHR = () => {
  // 用户登陆信息
  const user = {
    token: window.sessionStorage.token
  }

  const sucFun = res => {
    return res.data
  }

  const errFun = (res) => {
    let r = {
      msg: '请求失败,请稍后重试！'
    }
    return r
  }

  return { user, sucFun, errFun }
}
// 请求拦截
// let loadinginstace
// axiosX.interceptors.request.use((config) => {
//   loadinginstace = Loading.service({ fullscreen: true })
//   return config
// }, error => {
//   loadinginstace.close()
//   Message.error({
//     message: '加载超时'
//   })
//   return Promise.reject(error)
// })

// axiosX.interceptors.response.use((data) => {
// loadinginstace.close()
//   return data
// }, error => {
// loadinginstace.close()
// Message.error({
//   message: '加载失败'
// })
//   return Promise.reject(error)
// })

// POST
const axiosPost = function (url = '', data = {}) {
  let { user, sucFun, errFun } = XHR()
  let reqData = qs.stringify({ ...user, ...data })
  return axiosX.post(url, reqData).then(sucFun).catch(errFun)
}

// GET
const axiosGet = function (url = '', data = {}) {
  let { user, sucFun, errFun } = XHR()
  let params = {
    params: { ...user, ...data }
  }
  return axiosX.get(url, params).then(sucFun).catch(errFun)
}

const axiosRefund = function (url = '', data = {}) {
  let { user, sucFun, errFun } = XHR()
  let reqData = qs.stringify({ ...user, ...data })
  return reFundPost.post(url, reqData).then(sucFun).catch(errFun)
}

export default (Vue) => {
  Vue.prototype.$axios = axios
  Vue.prototype.$axiosGet = axiosGet
  Vue.prototype.$axiosPost = axiosPost
  Vue.prototype.$axiosRefund = axiosRefund
  Vue.prototype.$baseApi = SERVICE_URL
  Vue.prototype.$WsApi = WEBSOCKET_URL
  Vue.axios = axios
  Vue.axiosPost = axiosPost
  Vue.axiosGet = axiosGet
  Vue.axiosRefund = axiosRefund
}
export {axiosGet, axiosPost, axiosRefund}
