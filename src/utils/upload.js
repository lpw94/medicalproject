import { axiosGet } from '@/utils/axios'
import { getSign } from '@/api/app'

/**
 * 上传文件
 * @param file 文件
 * @param successCallback 上传成功回调，参数为文件url
 * @param progressCallback 上传进度回调，参数为当前进度
 * 传入的回调函数使用箭头函数，可以避免 this 指向改变
 */
const upload = function (file, successCallback, progressCallback) {
  axiosGet(getSign, {}).then(res => {
    if (+res.code === 1000) {
      // console.log(file)
      let saveName = new Date().getTime() + 300000 + '.' + file.name.substr(file.name.lastIndexOf('.') + 1)
      const fd = new FormData()
      const { host, accessid, policy, signature, dir } = res.data
      fd.append('OSSAccessKeyId', accessid)
      fd.append('policy', policy)
      fd.append('signature', signature)
      fd.append('key', dir + '/' + saveName)
      fd.append('success_action_status', 200)
      fd.append('file', file, saveName)
      const xhr = new XMLHttpRequest()
      xhr.open('post', host, true)
      // 上传进度回调
      xhr.upload.addEventListener('progress', (evt) => {
        let progress = Math.round((evt.loaded) * 100 / evt.total)
        progressCallback && progressCallback(progress)
      }, false)
      // 上传完成回调
      xhr.addEventListener('load', (e) => {
        if (e.target.status !== 200) {
        } else {
          let imgUrl = host + '/' + dir + '/' + saveName
          successCallback && successCallback(imgUrl)
        }
      }, false)
      // 发送请求
      xhr.send(fd)
    } else {
      // 获取签名失败
      console.log('error')
    }
  })
}

export { upload }
export default (Vue) => {
  Vue.prototype.$upload = upload
  Vue.upload = upload
}
