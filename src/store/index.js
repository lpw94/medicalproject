import Vue from 'vue'
import Vuex from 'vuex'
import transition from '@/store/modules/transition'
import getprize from '@/store/modules/getprize'

// 注册 Vuex
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    transition,
    getprize
  }
})
