const state = {
  direction: 'go'
}

const mutations = {
  updateDirection (state, isGo) {
    state.direction = isGo
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
