const state = {
  prizeInfo: {
    activeId: '',
    marketId: ''
  }
}

const getters = {
  getActiveInfo (state) {
    return state.prizeInfo
  }
}

const mutations = {
  setActiveInfo (state, data) {
    state.prizeInfo = {
      activeId: data.activeId,
      marketId: data.marketId
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}
