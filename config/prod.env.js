'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SERVICE_ENV: JSON.stringify(process.env.SERVICE_ENV)
}
